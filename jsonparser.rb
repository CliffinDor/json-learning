#!/usr/bin/env ruby
#These comments will be rather more verbose than normal, as I try and explain clearly what I'm trying to do and why.  Some of this information should be obvious to an experienced programmer.  I am not one of those.  As such, I will err on the side of over-explaining, so that if I'm wrong about something, it will come to light sooner rather than later.
require 'json'


#This defines "nato" as "the contents of json sample.json"
nato = File.read('json sample.json')
#This defines 'obj' as the interpretation of 'nato'
obj = JSON.parse(nato)

puts "variable obj is a #{obj.class}"
puts "variable obj has #{obj.size} elements"

# show the structure: `keys` is a method of the Hash class
#the method obj.keys.each do says "for each value in obj, do the thing in line 18
puts "\nThe brute force display method"
obj.keys.each do |key|
  puts "\t#{key} equals #{obj[key]}"
end

# show the structure, but now let's do it the Hash method
# Simple as anything, for each key, return the value associated with that key.
puts "\nThe Hash structure method"
obj.each do |key, value|
  puts "\t#{key} equals #{value}"
end

#This looks a bit more complicated but actually isn't that bad.  It looks like it reads "For each key, return key = value, but if value is an enumerable instead of a single item(I'm not sure about this part), print "key contains values X, Y, and Z, but each on a new line"
puts "\nShow subvalues individually"
obj.each do |key, value|
  if value.is_a? Enumerable
    puts "\t#{key} contains values:"
    value.each do |member|
      puts "\t#{member}"
    end
  else
    puts "\t#{key} equals #{value}"
  end
end

#This is intended to print each item from json sample.json.  Unfortunately, it's printing "name" and "type" as well as the things I want it to print.
obj.each do |blah|
	puts blah
end

#This is supposed to print the fourth item in the obj array, but is returning nil.  I don't really understand why.  The plan was I would tell Ruby to print only the things I wanted it to print, so it wouldn't print "name" and "types".
# nil because the Hash contains two elements, not three, as seen above
puts obj[3].inspect

#obj.keys.each gets you just the key instead of the key:value 
